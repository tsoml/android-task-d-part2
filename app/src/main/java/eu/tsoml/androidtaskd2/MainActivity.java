package eu.tsoml.androidtaskd2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Button addTownButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        addTownButton = findViewById(R.id.addContactButton);

        List<Contact> contacts = new ArrayList<>(Arrays.asList(
                new Contact("John Snow", "sample@gmail.com", "USA", "+911123456"),
                new Contact("John Smith", "sample@gmail.com", "USA", "+911123456"),
                new Contact("Tim Apple", "sample@gmail.com", "USA", "+911123456")
        ));


        final ContactsAdapter adapter = new ContactsAdapter(contacts);

        adapter.setListener(new ContactsAdapterClickListener() {
            @Override
            public void onClicked(Contact contact) {
                Intent intent = new Intent(MainActivity.this, ContactInfoActivity.class);
                intent.putExtra("contact", contact);
                startActivity(intent);


            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        addTownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.addTown(new Contact("New Contact", "sample@gmail.com", "USA", "+911123456"));
            }
        });
    }


}
