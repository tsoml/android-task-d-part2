package eu.tsoml.androidtaskd2;

public interface ContactsAdapterClickListener {
    void onClicked(Contact contact);
}
