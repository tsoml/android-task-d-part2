package eu.tsoml.androidtaskd2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ContactInfoActivity extends AppCompatActivity {


    TextView nameTextView;
    TextView emailTextView;
    TextView addressTextView;
    TextView phoneTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);

        Intent intent = getIntent();
        Contact contact = (Contact) intent.getSerializableExtra("contact");

        nameTextView = findViewById(R.id.nameTextView);
        emailTextView = findViewById(R.id.emailTextView);
        addressTextView = findViewById(R.id.addressTextView);
        phoneTextView = findViewById(R.id.phoneTextView);

        nameTextView.setText(contact.getName());
        emailTextView.setText(contact.getEmail());
        addressTextView.setText(contact.getAddress());
        phoneTextView.setText(contact.getPhone());

    }
}
